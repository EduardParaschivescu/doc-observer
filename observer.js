const fs = require('fs')

const express = require('express')
const app = express()
const appPort = 8000
const doc = express()
const docPort = 80;

doc.use(express.static('build'))
doc.listen(docPort, () => {
	console.log(`Doc listening on port ${docPort}`)
})

app.use(express.static('public'))
app.get('/refresh', (req, res) => {
	refresh();
	res.send('Hello World')
})
app.listen(appPort, () => {
	console.log(`Example app listening on port ${appPort}`)
})

function refresh(){
	var data = dataRead();
	const template = fs.readFileSync('public/template.html', 'utf8')
	var buildElement = template;
	//-------------------------------------
	buildElement = buildElement.replace("{{DATA_REFRESH}}", data.indices.DATA_REFRESH)
	buildElement = buildElement.replace("{{DATA_REFRESH_LAST}}", data.indices.DATA_REFRESH_LAST)
	buildElement = buildElement.replace("{{DATA_BUILDS}}", data.indices.DATA_BUILDS)
	buildElement = buildElement.replace("{{DATA_BUILDS_LAST}}", data.indices.DATA_BUILDS_LAST)
	buildElement = buildElement.replace("{{DATA_COMMITS}}", data.indices.DATA_COMMITS)
	buildElement = buildElement.replace("{{DATA_COMMITS_LAST}}", data.indices.DATA_COMMITS_LAST)
	//-------------------------------------
	var triggerTemplate = "<tr><td>{{0}}</td><td>{{1}}</td><td>{{2}}</td><td>{{3}}</td></tr>"
	var triggerHTML = "";
	for(var i = 0; i < data.triggers.length; i++){
		var newString = `${triggerTemplate}`
		newString = newString.replace("{{0}}", data.triggers[i][0]);
		newString = newString.replace("{{1}}", data.triggers[i][1]);
		newString = newString.replace("{{2}}", data.triggers[i][2]);
		newString = newString.replace("{{3}}", data.triggers[i][3]);
		triggerHTML += newString;
	}	 
	buildElement = buildElement.replace("{{DATA_TRIGGERS}}", triggerHTML);
	//-------------------------------------
	var logTemplate ="<tr><td>{{0}}</td><td>{{1}}</td><td>{{2}}</td></tr>";
	var logsHTML = "";
	for(var i = 0; i < data.logs.length; i++){
		var newString = `${logTemplate}`
		newString = newString.replace("{{0}}", data.logs[i][0]);
		newString = newString.replace("{{1}}", data.logs[i][1]);
		newString = newString.replace("{{2}}", data.logs[i][2]);
		logsHTML += newString;
	}	 
	buildElement = buildElement.replace("{{DATA_LOGS}}", logsHTML);
	//-------------------------------------
	fs.writeFileSync('public/index.html', buildElement);
}

function dataRead(){
	var data = {}

	//-----------------------------------------------------------------------
	const logs = fs.readFileSync('data/logs.data', 'utf8').split('\r\n')
	var filteredLogs = [];
	for(var id = logs.length - 10; id < logs.length; id++){
		console.log(`id : ${id}`)
		var log = logs[id].split("||")
		filteredLogs.push([log[0].trim(), log[1].trim(), log [2].trim()]);
	}
	data.logs = filteredLogs.reverse();
	//-----------------------------------------------------------------------

	//-----------------------------------------------------------------------
	const triggers = fs.readFileSync('data/triggers.data', 'utf8').split('\r\n')
	var filteredTriggers = [];
	for(var id = triggers.length - 10; id < triggers.length; id++){
		var trigger = triggers[id].split("||")
		filteredTriggers.push([trigger[0].trim(), trigger[1].trim(), trigger[2].trim(), trigger[3].trim()]);
	}
	data.triggers = filteredTriggers.reverse();
	//-----------------------------------------------------------------------

	//-----------------------------------------------------------------------
	const indices = fs.readFileSync('data/index.data', 'utf8').split('\r\n')
	var indexList = {};
	for(var id = 0; id < indices.length; id++){
		var index = indices[id].split("=")
		indexList[index[0].trim()] = index[1].trim()	
	}
	data.indices = indexList;
	//-----------------------------------------------------------------------

	return data;
}